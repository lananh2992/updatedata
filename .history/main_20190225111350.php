<?php

if(isset($argv[1]) && !empty($argv[1])){
	$lg = $argv[1];
}
else{
	echo "manque argument";
	exit;
}
require "source_".$lg.".php";
try
		{
    
            if( !empty($arr_distinc) && !empty($arr_local) ){
                $bdd_distinc	= new PDO($arr_distinc[0],$arr_distinc[1], $arr_distinc[2]);
                $bdd_local		= new PDO($arr_local[0],$arr_local[1], $arr_local[2]);
            }else{
                log_simple('$arr_distinc and $arr_local are empty');
            }

			log_simple('Connexion to data base simple list');
		//	var_dump($simpleLists); 
			if( !empty($simpleLists)){
				foreach( $simpleLists as $sList){
					log_simple('if table was found');
					$mrSql 		= "SHOW TABLES LIKE :table_name";
					$mrStmt		= $bdd_distinc->prepare($mrSql);
					$mrStmt->bindParam(":table_name",$sList, PDO::PARAM_STR);
					$sqlResult	= $mrStmt->execute();
					if($sqlResult){
						$row  = $mrStmt->fetch(PDO::FETCH_NUM);
						if($row[0]){
							$sqlResult->closeCursor();
							log_simple('Table :'.$sList);
							$sql 		= "SELECT MAX(posted) FROM :table_name";
							$statement	= $bdd_distinc->prepare($sql);
							$statement->bindParam(':table_name', $sList, PDO::PARAM_STR); 
							$statement->execute();
							$maxDate	= $statement->fetch();
							$id 		= 0;
							while(1){
							//$statement->closeCursor;
								log_simple('Connexion data local');
							
								$sql1 			= "SELECT id, site_id, url, anchor, title_html, content_html, posted, nickname_html, created FROM :table_name WHERE posted >= :posted AND id > :id ORDER BY id ASC LIMIT 500000";
								$statementLocal	= $bdd_local->prepare($sql1);
								$statementLocal->bindParam(':table_name', $sList, PDO::PARAM_STR);
								$statementLocal->bindParam(':id', $id, PDO::PARAM_INT);
								$statementLocal->execute();
								$elements		= $statementLocal->fetchAll();
								log_simple('Update data to bdd distinc');
									
								while($elements){
									$site_id		= $elements['site_id'];
									if( !empty($site_id)){
										$url			= $elements['url'];
										$anchor			= $elements['anchor'];
										$title_html		= $elements['title_html'];
										$content_html	= $elements['content_html'];
										$posted			= $elements['posted'];
										$nickname_html	= $elements['nickname_html'];
										$created		= $elements['created'];
										$id				= $elements['id'];
										$req = $bdd_distinc->prepare('INSERT IGNORE INTO :table_name(site_id, url, anchor, title_html, content_html, posted, nickname_html, created) VALUES(:site_id, :url, :anchor, :title_html, :content_html, :posted, :nickname_html, :created)');
										$req->bindParam(':table_name', $sList, PDO::PARAM_STR);
										$req->execute(array(
											'site_id' 	 	 =>	$site_id,
											'url'	  		 =>	$url,
											'anchor'  	 	 =>	$anchor,
											'title_html' 	 =>	$title_html,
											'content_html' 	 =>	$content_html,
											'posted' 		 =>	$posted,
											'nickname_html'  =>	$nickname_html,
											'created'		 =>	$created,
										));
									$req->closeCursor;
									}else{
										log_simple('Site_id is empty , table : '.$sList);
										
										break;
									}
								}
								//nbr_element = 0; break;
								if($elements == 0){
									break;
								}
							}
							$statementLocal->closeCursor;
							$bdd_distinc = null;
							$bdd_local = null;
							log_simple('Up date data : '.$sList);
							
						}
							
					}else{
						log_simple('table was not found');
						
						continue;
					}
				}
			}else{
				log_simple('List table simple is empty');
				
			}

			///////////////////////////////////////
			////////////////////////DualLists /////
			//////////////////////////////////////
			//Logger::verbose('Connexion to data base dual list');
			
			if( !empty($dualLists)){
				foreach( $dualLists as $sList => $table_distinc ){
					log_simple('if table was found data base distinc');
					$mrSql 		= "SHOW TABLES LIKE :table_name";
					$mrStmt		= $bdd_distinc->prepare($mrSql);
					$mrStmt->bindParam(":table_name",$table_distinc, PDO::PARAM_STR);
					$sqlResult	= $mrStmt->execute();
					if($sqlResult){
						$row  = $mrStmt->fetch(PDO::FETCH_NUM);
						if($row[0]){
							$sqlResult->closeCursor();
							log_simple('Table :'.$table_distinc);
							
							$sql 		= "SELECT MAX(posted) FROM :table_name";
							$statement	= $bdd_distinc->prepare($sql);
							$statement->bindParam(':table_name', $table_distinc, PDO::PARAM_STR); 
							$statement->execute();
							$maxDate	= $statement->fetch();
							$id 		= 0;
							while(1){
							//$statement->closeCursor;
								log_simple('Connexion data local');
							
								$sql1 			= "SELECT id, site_id, url, anchor, title_html, content_html, posted, nickname_html, created FROM :table_name WHERE posted >= :posted AND id > :id ORDER BY id ASC LIMIT 500000";
								$statementLocal	= $bdd_local->prepare($sql1);
								$statementLocal->bindParam(':table_name', $sList, PDO::PARAM_STR);
								$statementLocal->bindParam(':id', $id, PDO::PARAM_STR);
								$statementLocal->execute();
								$elements		= $statementLocal->fetchAll();
									log_simple('Update data to bdd distinc');
									
								while($elements){
									$site_id		= $elements['site_id'];
									if( !empty($site_id)){
										$url			= $elements['url'];
										$anchor			= $elements['anchor'];
										$title_html		= $elements['title_html'];
										$content_html	= $elements['content_html'];
										$posted			= $elements['posted'];
										$nickname_html	= $elements['nickname_html'];
										$created		= $elements['created'];
										$id				= $elements['id'];
										$req = $bdd_distinc->prepare('INSERT IGNORE INTO :table_name(site_id, url, anchor, title_html, content_html, posted, nickname_html, created) VALUES(:site_id, :url, :anchor, :title_html, :content_html, :posted, :nickname_html, :created)');
										$req->bindParam(':table_name', $table_distinc, PDO::PARAM_STR);
										$req->execute(array(
											'site_id' 	 	 =>	$site_id,
											'url'	  		 =>	$url,
											'anchor'  	 	 =>	$anchor,
											'title_html' 	 =>	$title_html,
											'content_html' 	 =>	$content_html,
											'posted' 		 =>	$posted,
											'nickname_html'  =>	$nickname_html,
											'created'		 =>	$created,
										));
									$req->closeCursor;
									}else{
										log_simple('Site_id is empty , table : '.$sList);
										
										break;
									}
								}
								//nbr_element = 0; break;
								if($elements == 0){
									break;
								}
							}
							$statementLocal->closeCursor;
							$bdd_distinc = null;
							$bdd_local = null;
							log_simple('Up date data : '.$table_distinc);
							
						}
							
					}else{
						log_simple('table was not found');
						
						continue;
					}
				}
			}else{
				log_simple('List table dual is empty');
				
			}
		}
	catch( Exception $e){
			die('Erreur: ' .$e->getMessage());
}

function log_simple($txt){
	$fp = fopen ("./simple_log.log", "a");
	fputs ($fp, date('Y-m-d H:i:s')." - ".$txt."\n");
	fclose ($fp);
	}


?>

   
